- [系统变量](#系统变量)
- [测试工程](#测试工程)
- [添加命令](#添加命令)
- [加载.arx](#加载arx)
- [移植到 vs2010](#移植到-vs2010)
- [vs2019 敲代码](#vs2019-敲代码)

## 安装环境

1. Acad2008

2. vs2005

3. vs2010,vs2010sp1(补丁)

4. vs2019(最终会在这里敲代码)
   这里要安装 MFC,如下:
   **通用 Windows 平台开发**

   适用于最新 v142 生成工具的 C++ ATL(x86 和...
   适用于最新 v142 生成工具的 C++ MFC (x86...
   Windows 通用 CRT SDK
   MSVC v140 - vS 2015C++生成工具(v14.00)
   .NET Framework 3.5 开发工具
   C++ ATL v141 生成工具(x86 & x64)
   C++ MFC v141 生成工具(x86 & x64)
   C++ v14.20 ATL for v142 生成工具(x86 & x64)
   C++ v14.20 MFC for v142 生成工具(x86 和 x64)

5) 非必要的 vs 插件:

   Visual AssistX 番茄助手,我用是人家翻译的一个中文版,这个是用来敲代码的时候有个提示...

   GhostDoc_v4.9 ///可以快速出现头函数注释

   IndentGuide 令代码括号有条参照线...

## 安装 ObjectArx SDK

SDK 和 wizards(vs 向导)和地址:
忘记从什么时候开始 cad 的 wizards 和 sdk 分离的,向导文件给 vs 直接创建一个 arx 工程用的.
仅仅为了方便,并不是必须,可能因此桌子把他们分离了.
而浩辰中望等等没有 wizards 的,所以你必须掌握没有 wizards 来创建一个工程.

向导文件的说明:
{
向导文件的下载地址: https://www.autodesk.com/developer-network/platform-technologies/autocad
拖拉到 arx 最下面
Tools
ObjectARX Wizards and AutoCAD .NET Wizards for AutoCAD and Visual Studio
ARX 位置下载.

     官方向导文件的github开源地址: https://github.com/ADN-DevTech/ObjectARX-Wizards

}

我看了一下 github 上面也有一个**2000-2015sdk**的工程: https://github.com/kevinzhwl/ObjectARXCore

[2021-64 位版本](http://download.autodesk.com/esd/objectarx/2021/objectarx_for_autocad_2021_win_64bit_dlm.sfx.exe)
[2021-向导](https://github.com/ADN-DevTech/ObjectARX-Wizards/raw/ForAutoCAD2021/ObjectARXWizardsInstaller/ObjectARXWizard2021.zip)

[2020-64 位版本](http://download.autodesk.com/esd/objectarx/2020/objectarx_for_autocad_2020_win_64_bit.sfx.exe)
[2020-向导](https://github.com/ADN-DevTech/ObjectARX-Wizards/raw/ForAutoCAD2020/ObjectARXWizardsInstaller/ObjectARXWizard2020.zip)

[2019-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2019/Autodesk_ObjectARX_2019_Win_64_and_32_Bit.sfx.exe)
[2019-向导](http://damassets.autodesk.net/content/dam/autodesk/www/adn/files/ObjectARXWizard2019.zip)

[2018-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2018/Autodesk_ObjectARX_2018_Win_64_and_32_Bit.sfx.exe)
[2018-向导](http://images.autodesk.com/adsk/files/ObjectARXWizards-2018.zip)

[2017-32 和 64 位版本](http://download.autodesk.com/esd/objectarx/2017/Autodesk_ObjectARX_2017_Win_64_and_32_Bit.sfx.exe)
[2017-向导](http://images.autodesk.com/adsk/files/ObjectARXWizards-2017.zip)

[2016-32 和 64 位版本](http://download.autodesk.com/esd/objectarx/2016/Autodesk_ObjectARX_2016_Win_64_and_32_Bit.exe)

[2015-32 和 64 位版本](http://download.autodesk.com/esd/objectarx/2015/Autodesk_ObjectARX_2015_Win_64_and_32_Bit.exe)

[2014-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2014/Autodesk_ObjectARX_2014_Win_64_and_32Bit.sfx.exe)
[2014-帮助文件](http://download.autodesk.com/esd/objectarx/2014/Autodesk_ObjectARX_2014_Documentation.sfx.exe)

[2013-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2013/ObjectARX_2013_Win_64_and_32Bit.exe)
[2013-帮助文件](http://download.autodesk.com/esd/objectarx/2013/ObjectARX_2013_Documentation.exe)

[2012-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2012/ObjectARX_2012_Win_64_and_32Bit.exe)

[2011-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2011/ObjectARX_2011_Win_64_and_32Bit.exe)

[2010-32 位和 64 位版本](http://download.autodesk.com/akdlm/esd/dlm/objectarx/ObjectARX_2010_Win_64_and_32Bit.exe)

[2009-32 位和 64 位版本](http://download.autodesk.com/esd/objectarx/2009/ObjectARX_2009_Win_64_and_32Bit.exe)

[2008-32 位版本](http://download.autodesk.com/esd/objectarx/2008/ObjectARX_2008_32Bit.exe)
[2008-64 位版本](http://download.autodesk.com/esd/objectarx/2008/ObjectARX_2008_64Bit.exe)

[2007-Core](http://download.autodesk.com/esd/objectarx/2007/Arx_Core.exe)
[2007-SDK](http://download.autodesk.com/esd/objectarx/2007/Arx_All.exe)

[2006-Core](http://download.autodesk.com/WebPub/autocad/oarx2006/Arx_Core.exe)
[2006-SDK](http://download.autodesk.com/WebPub/autocad/oarx2006/Arx_All.exe)

[2005-Core](http://download.autodesk.com/WebPub/Developer/autocad/Arx_Core2005.exe)
[2005-SDK](http://download.autodesk.com/WebPub/Developer/autocad/Arx_All2005.exe)

[2004-Core](http://download.autodesk.com/WebPub/autocad/oarx/arx_core.exe)
[2004-SDK](http://download.autodesk.com/WebPub/autocad/oarx/arx_sdk.exe)

[2002-Core](http://download.autodesk.com/pub/objectarx/objectarx_2002/K030.arx.plus.core.zip)
[2002-SDK](http://download.autodesk.com/pub/objectarx/objectarx_2002/K030.arx.plus.all.zip)

[2000-Core](http://download.autodesk.com/pub/objectarx/ObjectArxCore.exe)

[2000-SDK](http://download.autodesk.com/pub/objectarx/ObjectArxSDK.exe)

[R14-SDK](http://download.autodesk.com/Pub/developer/sdk/obarxsdk.exe)

## 安装向导 ObjectArx 2008 ObjARXWiz

ObjectArx 2008 SDK 是一些可以直接引用的文件,而 wizards 是安装在 vs,方便新手入门学习.(如果此时你需要非向导配置工程,参考我写的[Grx 配置篇](https://www.cnblogs.com/JJBox/p/10961690.html))

先做一个**.reg 注册表**文件,加入下面的语句,然后运行这个注册表.

```regdit
Windows Registry Editor Version 5.00
[HKEY_CLASSES_ROOT\Msi.Package\shell\runas]
@="以管理员运行"
[HKEY_CLASSES_ROOT\Msi.Package\shell\runas\command]
@="msiexec /i \"%1\""
```

然后打开 C:\ObjectARX 2008\utils\ObjARXWiz 目录(其他高版本的 SDK 要另外下载这个向导的,我仍然喜欢放在.\utils\ObjARXWiz 目录下)

再右键管理员模式运行 ArxWizards.msi

![img](/图片/1285775-20210201113510506-70191387.png)

这样在新建 C++项目的时候会出现 ObjectARX 项目,

如果新建出错,也就是你没有按照上面修改注册表管理员方式安装.

# 系统变量

![img](/图片/1285775-20210201204052109-1646550140.png)

修改系统环境变量,增加 cad 的路径和 SDK 路径到里面去,这样可以很大程度方便其后的配置...

尤其是工程在不同的人手上打开的时候能够正确识别.(公司化),防止在工程上面将路径写死...

注意一定要\结尾....

```
ACAD2008            C:\Program Files (x86)\AutoCAD 2008\
ObjectARXSDK2008    G:\sdkAndvc\ObjectARX\ObjectARX 2008\
```

其他版本的 SDK 路径请你自行设置.

这样就可以在 vs 工程属性页中填入并识别上,即使是 vs2005 都是可以的.

![img](/图片/1285775-20210201224912576-1413078284.png)

vs2010+才可以通过内置的宏浏览器找对应的路径.

![img](/图片/1285775-20200501145506353-1165118087.png)

# 测试工程

![img](/图片/1285775-20200501025256183-2117402913.png)

![img](/图片/1285775-20200501025228634-1998085728.png)

![img](/图片/1285775-20200501025345926-1100440214.png)

```
这些是当前的项目设置：
您注册的开发人员符号：
纯ObjectARX应用程序
没有MFC支持
不是COM服务器
不是.NET混合管理模块
从任何窗口单击完成以接受当前设置。
创建项目后，请参阅项目的readme.txt文件，了解有关生成的项目功能和文件的信息。
实现_DEBUG预处理器符号
选择此选项可在调试项目配置中定义预处理器符号_DEBUG。 如果选择此选项，向导将在StdAfx.h中创建通常的_DEBUG解决方法，但在定义此符号时，不应包含Microsoft / Autodesk头。
```

输入你喜欢的前缀,然后是不是 DEBUG.我先不勾

![img](/图片/1285775-20200501025624537-1922955503.png)

```
项目类型：
ObjeCtARX（AutoCAD扩展名）
选择此选项可创建标准的ObjectARX应用程序
以便在AutoCAD中加载AutoCAD项目。

ObjectDBX（自定义对象启用程序）
选择此选项可创建标准的ObjectDBX自定义对象启用程序应用程序。自定义对象启用码应用程序定义自定义对象和自定义实体可以加载到所有RealDWG主机应用程序中，如AutoCAD/VoloView
```

第一个功能就是普通的功能,第二个是自定义图元用的.先用第一个入门吧.

![img](/图片/1285775-20200501025915190-616092490.png)

```
附加SDK：
OMF应用程序支持
选择此选项将强制向导创建OMF符合规范。
OMF 5.0版
OMF 5.x系列

地图API支持
选择此选项将强制向导包含映射标题和库。
地图2007 SDK
地图Spago SDK
```

都不选,下一步

![img](/图片/1285775-20200501030102872-527808963.png)

```
MFC支持：
无MFC支持
具有静态链接的MFC的常规DLL。
使用MFC共享DLL的常规DLL
使用MFC共享DLL的扩展DLL（建议用于MFC支持）
团AutoCAD MFC扩展支持
```

MFC 就是微软画面板的支持,所以要打钩.

![img](/图片/1285775-20200501030532074-1072754350.png)

```
COM服务器选项：

不是COM服务器：
实现COM服务器
   使用ATL实现COM服务器（推荐）
     对自定义对象使用ObjeCtARX AIL扩展

COM客户端选项：
导入QbjectDBX COM接口
导入AutoCAD COM接口
```

为了获取 cad 变量方便,导入 cad 的 com 接口.

![img](/图片/1285775-20200501030639792-94895138.png)

对.net 的支持,我虽然有 net 工程,但是这步我先不勾,日后再看看怎么改支持.

这样就可以了,按 Finish 完成.

按道理来说,现在你可以直接生成文件验证配置是否正确.....

若出现 **.h(头文件)** 不正确,你需要进行一次手动配置的检查.

```
1>d:\桌面\arxproject1\arxproject1\stdafx.h(97) : fatal error C1083: 无法打开包括文件:“arxHeaders.h”: No such file or directory
```

说明缺少配置:

![img](/图片/1285775-20210201214621416-371624051.png)

```
"$(ACAD2008SDK)inc-win32"
$(ACAD2008)acad.exe
```

若出现 **.lib(静态链接库)** 不正确,例如:

```
1>LINK : fatal error LNK1104: 无法打开文件“rxapi.lib”
```

说明缺少配置:

![img](/图片/1285775-20210201215427061-972738588.png)

\$(ACAD2008SDK)lib-win32

# 添加命令

点击工具栏向导

![img](/图片/1285775-20200501031122248-1827566558.png)

![img](/图片/1285775-20210201215953796-305107828.png)

右击,添加命令,一般来说改名称就好了.

然后在入口点 acrxEntryPoint.cpp 可以看见加入的命令.

![img](/图片/1285775-20210201220157970-1331353674.png)

其实那个 a>的添加命令就是添加了这几句而已,可能是因为宏难找的原因吧......再添加一个试试吧..

![img](/图片/1285775-20200501033442434-534424949.png)

嘿嘿.然后再试试复制粘贴,再看看 a>内是不是有?所以可以摆脱用 a>了呢!

![img](/图片/1285775-20200501034351362-1013712775.png)

加入警告语句:

```c++
AfxMessageBox(_T("this is a test command0."));
```

# 加载.arx

到这个的项目文件夹 win32\debug 目录中找到.arx 拖入 CAD 界面加载,或 ap 加载.再输入自己的命令就能运行了.

注意 vs 工程有很多 win32,x64 文件夹,因为微软就是那么奇葩...具体可能要了解一下 vs 的宏命令,设置输出的路径..

到此为止,vs2005 的 arx 工程就算是 OJBK 了..

不过,你是否嫌弃拖入加载和 ap 加载都很麻烦?

[调试]/[命令参数] 这个/ld 就是 load 的意思,所以直接 F5 启动调试,就会自动加载了.

![img](/图片/1285775-20210201225503480-1820563382.png)

需要注意几点:

1,在[调试]/[命令参数]上面,vs2005 不用/nologo 也没问题,

但是 vs2010 即使设置了[链接器]/[取消显示启动版权标识] 但是依然会有显示,

所以为了统一不显示,就都带上,调试时候才没有徽标.

2,在[调试]/[命令参数]上面, /ld 后面的路径必须是[链接器]/[输出文件]上面填写的路径,但是[输出文件]上面不能用引号...[命令参数]这里不用引号就会导致启动 cad 之后不自动加载,所以这里又必须带引号...

# 移植到 vs2010

先安装一个 [Daffodil.ENU](http://daffodil.codeplex.com/releases/view/55712) 详情看下面 e 大的文章:

1. [ObjectARX 多版本批量编译](https://www.cnblogs.com/edata/p/8016789.html)
2. [VS2010 编译更高平台 objectARX 程序](https://www.cnblogs.com/edata/p/8061127.html)
3. [ObjectARX 开发环境搭建工具条问题修复](https://www.cnblogs.com/edata/p/6821379.html)

用 vs2010 打开刚刚的 vs2005 工程,选择更新;

右击工程属性,所有配置,所有平台,修改拓展名为.arx,修改平台集.

![img](/图片/1285775-20210201153408726-1345403966.png)

1:选择 v80 工具集(没有的话,说明你没有看 e 大的文章)

2:如果没有配置后缀名,会出现这样错误,修改你的生成后缀名[配置属性]/[常规]/[目标拓展名]:arx

```
1>C:\Program Files (x86)\MSBuild\Microsoft.Cpp\v4.0\Microsoft.CppBuild.targets(990,5): warning MSB8012: TargetPath
(D:\桌面\JJBoxArx2008\Win32\Debug\JJBoxArx2008.dll) 与 Linker 的 OutputFile 属性值(D:\桌面\JJBoxArx2008\Win32\Debug\JJBJJBoxArx2008.arx)不匹配。
这可能导致项目生成不正确。若要更正此问题，请确保 $(OutDir)、$(TargetName) 和 $(TargetExt) 属性值与 %(Link.OutputFile) 中指定的值匹配。
```

若刚刚在 vs2005 设置过,以下仅需检查一下.

[配置属性]/[C/C++]/[附加包含目录]:

![img](/图片/1285775-20210201222256271-1359564799.png)

[配置属性]/[链接器]/[常规]/[附加库目录]:

![img](/图片/1285775-20210201222341108-1116158347.png)

否则出现 LNK1104: 无法打开文件

[链接器]/[输入]/[附加依赖项]

```
(注意一下,vs2005写法)
rxapi.lib acdb17.lib acge17.lib acad.lib acedapi.lib
(注意一下,vs2010写法)
rxapi.lib;acdb17.lib;acge17.lib;acad.lib;acedapi.lib
```

还是无法打开的话

```
vs2005就去看看[工具]/[选项]/[项目和解决方案]/[vc++目录]
vs2010就去看看右击项目配置属性/[vc++目录]
包含文件/包含目录 加入  D:\ObjectARX 2008\inc
库文件/库目录    加入  D:\ObjectARX 2008\lib
```

错误 error C1083: 无法打开包括文件:“map”: No such file or directory

```
用于“编辑并继续”的程序数据库 (/ZI)
设为空,删除->  公共语言运行时支持 (/clr)
启用c++异常    是 (/EHsc)
基本运行时检查->默认值
调用约定写入->    __cdecl (/Gd)
```

三、包含文件 这个做法在 vs2010 已经没有了, [工具]/[选项]/[项目和解决方案]/[VC++目录],如图所示：

包含文件(添加图中前两个目录即可) (这里的路径可自行设置到环境变量的路径)

```
D:\ObjectARX 2008\inc
D:\ObjectARX 2008\utils\brep\inc
```

![img](/图片/1285775-20200501042032290-1290555531.png)

库文件(这里的路径可自行设置到环境变量的路径)

```
D:\ObjectARX 2008\lib
D:\ObjectARX 2008\utils\brep\lib
```

![img](/图片/1285775-20200501042017909-1028322623.png)

# vs2019 敲代码

由于番茄助手不太好用的关系,所以我最后在 vs2019 打开 vs2010 的工程是不会提示升级的,

所以可以利用 vs2019 来敲代码.而且直接利用 vs2019 也可以调试 cad2008,妙~

(完)
