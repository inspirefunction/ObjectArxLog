- [修改 VS 程序文件令它自动配置](#修改-vs-程序文件令它自动配置)
  - [设置启动直接加载](#设置启动直接加载)
  - [设置输出文件](#设置输出文件)
    - [vs2005 设置 default.js](#vs2005-设置-defaultjs)
    - [vs2010 设置](#vs2010-设置)

以下文章可能存在旧信息,不如前篇描述得好,例如环境变量配置等等...

顺带一提, vs 切换.cpp 和.h 在 vs2019 用 ctrl+k o

# 修改 VS 程序文件令它自动配置

按照[e 大的教程](https://www.cnblogs.com/edata/p/6821379.html),修改 default.js 这个文件可以达到一劳永逸,令新建工程的时候自动配置上面所有的修改项,

win10 的路径:

```
C:\Program Files (x86)\Autodesk\ObjectARX Wizards for AutoCAD 2008\ArxAppWiz\Scripts\1033
```

```
C:\Program Files (x86)\Autodesk\ObjectARX Wizards for AutoCAD 2012\ArxAppWiz\Scripts\1033
```

win7 的路径:

```
C:\Program Files\Autodesk\ObjectARX Wizards for AutoCAD 2008\ArxAppWiz\Scripts\1033
```

```
C:\Program Files\Autodesk\ObjectARX Wizards for AutoCAD 2012\ArxAppWiz\Scripts\1033
```

## 设置启动直接加载

![img](/图片/1285775-20200504011324612-1594162906.png)

vs2005 和 vs2010 添加这一行:

```
DebugTool.CommandArguments ='/ld \"$(OutDir)\$(ProjectName)$(TargetExt)\"' ;
```

## 设置输出文件

![img](/图片/1285775-20200504011358328-66133847.png)

vs2005 和 vs2010 修改这一行:

```
LinkTool.OutputFile ='\"$(OutDir)\\$(ProjectName).arx\"' ;
```

### vs2005 设置 default.js

```javascript
//- Copyright (c) Autodesk, Inc. All rights reserved.
//- by Cyrille Fauvel - Autodesk Developer Technical Services

function OnFinish(selProj, selObj) {
  try {
    var strProjectPath = wizard.FindSymbol('PROJECT_PATH')
    var strProjectName = wizard.FindSymbol('PROJECT_NAME')
    wizard.AddSymbol('UPPER_CASE_PROJECT_NAME', strProjectName.toUpperCase())

    wizard.AddSymbol('SAFE_PROJECT_NAME', CreateSafeName(strProjectName))
    wizard.AddSymbol(
      'UPPER_CASE_SAFE_PROJECT_NAME',
      CreateSafeName(strProjectName.toUpperCase()),
    )

    selProj = CreateCustomProject(strProjectName, strProjectPath)
    AddConfigs(selProj, strProjectName)
    AddFilters(selProj)

    var InfFile = CreateCustomInfFile()
    AddFilesToCustomProj(selProj, strProjectName, strProjectPath, InfFile)
    PchSettings(selProj)
    InfFile.Delete()

    selProj.Object.Save()

    var bOmf = wizard.FindSymbol('OMF_APP')
    if (bOmf) MakeOmfResourceProject(selProj, strProjectPath, strProjectName)
  } catch (e) {
    if (e.description.length != 0) SetErrorInfo(e)
    return e.number
  }
}

//-----------------------------------------------------------------------------
function CreateCustomProject(strProjectName, strProjectPath) {
  try {
    var strProjTemplatePath = wizard.FindSymbol('PROJECT_TEMPLATE_PATH')
    var strProjTemplate = ''
    strProjTemplate = strProjTemplatePath + '\\default.vcproj'

    var Solution = dte.Solution
    var strSolutionName = ''
    if (wizard.FindSymbol('CLOSE_SOLUTION')) {
      Solution.Close()
      strSolutionName = wizard.FindSymbol('VS_SOLUTION_NAME')
      if (strSolutionName.length) {
        var strSolutionPath = strProjectPath.substr(
          0,
          strProjectPath.length - strProjectName.length,
        )
        Solution.Create(strSolutionPath, strSolutionName)
      }
    }

    var strProjectNameWithExt = ''
    strProjectNameWithExt = strProjectName + '.vcproj'

    var oTarget = wizard.FindSymbol('TARGET')
    var prj
    if (wizard.FindSymbol('WIZARD_TYPE') == vsWizardAddSubProject) {
      //----- vsWizardAddSubProject
      var prjItem = oTarget.AddFromTemplate(
        strProjTemplate,
        strProjectNameWithExt,
      )
      prj = prjItem.SubProject
    } else {
      prj = oTarget.AddFromTemplate(
        strProjTemplate,
        strProjectPath,
        strProjectNameWithExt,
      )
    }
    return prj
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function AddConfigs(proj, strProjectName) {
  try {
    //- Win32
    AddConfig(proj, strProjectName, /*Debug*/ true, /*Win32*/ true)
    AddConfig(proj, strProjectName, /*Release*/ false, /*Win32*/ true)
    //- x64
    var plist = proj.ConfigurationManager.SupportedPlatforms.toArray()
    for (var i = 0; i < plist.length; i++) {
      if (plist[i] == 'x64') {
        proj.ConfigurationManager.AddPlatform('x64', 'x64', true)
        AddConfig(proj, strProjectName, /*Debug*/ true, /*x64*/ false)
        AddConfig(proj, strProjectName, /*Release*/ false, /*x64*/ false)
        break
      }
    }
  } catch (e) {
    throw e
  }
}

function AddConfig(proj, strProjectName, bDebug, bWin32) {
  try {
    var szRds = wizard.FindSymbol('RDS_SYMB')
    var bArxAppType = wizard.FindSymbol('APP_ARX_TYPE')
    var bOmfAppType = wizard.FindSymbol('OMF_APP')
    var bMfcExtDll = wizard.FindSymbol('MFC_EXT_SHARED')
    var bSharedMfc = wizard.FindSymbol('MFC_REG_SHARED') || bMfcExtDll
    var bMfcSupport = wizard.FindSymbol('MFC_REG_STATIC') || bSharedMfc
    var bUseAtl = wizard.FindSymbol('ATL_COM_SERVER')
    var bComServer = wizard.FindSymbol('STD_COM_SERVER') || bUseAtl
    var bDotNetModule = wizard.FindSymbol('DOTNET_MODULE')
    var bImplDebug = wizard.FindSymbol('IMPL_DEBUG')
    var szUprProjectName = CreateSafeName(strProjectName.toUpperCase())

    var cfgName = bDebug ? 'Debug' : 'Release'
    cfgName += '|' + (bWin32 ? 'Win32' : 'x64')
    var config = proj.Object.Configurations(cfgName)
    if (config == null) return
    config.ConfigurationType = typeDynamicLibrary
    config.IntermediateDirectory = '$(PlatformName)\\$(ConfigurationName)'
    config.OutputDirectory =
      '$(SolutionDir)$(PlatformName)\\$(ConfigurationName)'
    config.CharacterSet = charSetUnicode
    config.useOfMfc = bMfcSupport ? useMfcDynamic : useMfcStdWin
    config.useOfAtl = bUseAtl ? useATLDynamic : useATLNotSet
    config.ManagedExtensions = bDotNetModule

    var CLTool = config.Tools('VCCLCompilerTool')
    CLTool.DebugInformationFormat = /*debugOption.*/ bDebug
      ? bWin32
        ? debugEditAndContinue
        : debugEnabled
      : debugDisabled
    CLTool.WarningLevel = warningLevel_1
    CLTool.TreatWChar_tAsBuiltInType = true
    CLTool.Detect64BitPortabilityProblems = true
    CLTool.ForceConformanceInForLoopScope = true
    CLTool.Optimization = bDebug ? optimizeDisabled : optimizeMaxSpeed
    CLTool.MinimalRebuild = bDebug
    CLTool.StringPooling = !bDebug
    CLTool.BasicRuntimeChecks = bDebug
      ? runtimeBasicCheckAll
      : runtimeBasicCheckNone
    CLTool.BufferSecurityCheck = true
    CLTool.RuntimeLibrary = rtMultiThreadedDLL
    CLTool.RuntimeTypeInfo = true
    CLTool.UsePrecompiledHeader = /*pchCreateUsingSpecific*/ /*pchGenerateAuto*/ pchUseUsingSpecific
    //CLTool.PrecompiledHeaderFile ="StdAfx.h" ;
    //- AD_UNICODE UNICODE _UNICODE are added either by .h or setting unicode at the project level
    //- We do not add _DEBUG anymore to avoid problems on Release AutoCAD platform
    CLTool.PreprocessorDefinitions += bWin32 ? 'WIN32' : '_WIN64'
    CLTool.PreprocessorDefinitions +=
      ';_WINDOWS;' + szUprProjectName + '_MODULE'
    CLTool.PreprocessorDefinitions += bDebug ? '' /*;_DEBUG*/ : ';NDEBUG'
    CLTool.PreprocessorDefinitions += bImplDebug && bDebug ? ';_DEBUG' : ''
    CLTool.PreprocessorDefinitions += bMfcExtDll ? ';_AFXEXT' : ''
    CLTool.PreprocessorDefinitions += bArxAppType ? ';_ACRXAPP' : ';_DBXAPP'
    if (bOmfAppType) {
      CLTool.PreprocessorDefinitions += ';_OMFAPP;USE_ACAD_MODELER'
      CLTool.PreprocessorDefinitions += bArxAppType ? '' : ';DBX'
      CLTool.AdditionalOptions += '-Zm1000'
    }
    if (bDotNetModule) {
      CLTool.CompileAsManaged = /*managedAssembly*/ managedAssemblyOldSyntax
      CLTool.Detect64BitPortabilityProblems = false
      CLTool.BasicRuntimeChecks = /*basicRuntimeCheckOption.*/ runtimeBasicCheckNone
      CLTool.DebugInformationFormat = /*debugOption.*/ bDebug
        ? debugEnabled
        : debugDisabled
      CLTool.MinimalRebuild = false
    }

    var LinkTool = config.Tools('VCLinkerTool')
    LinkTool.LinkIncremental = linkIncrementalYes
    if (bDotNetModule)
      LinkTool.OutputFile = '$(outdir)/' + szRds + strProjectName + '.dll'
    else if (bArxAppType)
      LinkTool.OutputFile = '"$(OutDir)\\$(ProjectName).arx"'
    else LinkTool.OutputFile = '$(outdir)/' + szRds + strProjectName + '.dbx'
    LinkTool.LinkDLL = true
    if (bOmfAppType) LinkTool.EntryPointSymbol = 'DllMain'
    //LinkTool.ModuleDefinitionFile =strProjectName + '.def' ;
    LinkTool.SubSystem = subSystemWindows
    LinkTool.GenerateDebugInformation = bDebug
    LinkTool.ProgramDatabaseFile = bDebug
      ? '$(OutDir)/' + strProjectName + '.pdb'
      : ''
    LinkTool.ImportLibrary = '$(OutDir)/' + strProjectName + '.lib'
    LinkTool.TargetMachine = /*machineTypeOption.*/ bWin32
      ? machineX86
      : machineAMD64

    if (bComServer) {
      var MidlTool = config.Tools('VCMidlTool')
      MidlTool.MkTypLibCompatible = false
      MidlTool.TargetEnvironment = /*midlTargetEnvironment.*/ bWin32
        ? midlTargetWin32
        : midlTargetNotSet
      MidlTool.PreprocessorDefinitions = bDebug ? '' /*_DEBUG'*/ : 'NDEBUG'
      MidlTool.PreprocessorDefinitions += bImplDebug && bDebug ? '_DEBUG' : ''
      MidlTool.HeaderFileName = '$(InputName).h'
      MidlTool.InterfaceIdentifierFileName = strProjectName + '_i.c'
      MidlTool.ProxyFileName = strProjectName + '_p.c'
      MidlTool.GenerateStublessProxies = true
      MidlTool.GenerateTypeLibrary = true
      MidlTool.TypeLibraryName = '$(IntDir)/' + szRds + strProjectName + '.tlb'
      MidlTool.DLLDataFileName = ''
    }

    var RCTool = config.Tools('VCResourceCompilerTool')
    RCTool.Culture = rcEnglishUS
    RCTool.AdditionalIncludeDirectories = '$(IntDir)'
    RCTool.PreprocessorDefinitions = bImplDebug && bDebug ? '_DEBUG' : ''

    var regkey = new ActiveXObject('WScript.Shell')
    var strAcadVersion = ''
    try {
      strAcadVersion = regkey.RegRead(
        'HKEY_CURRENT_USER\\SOFTWARE\\Autodesk\\AutoCAD\\CurVer',
      )
    } catch (e) {
      strAcadVersion = 'R17.1'
    }
    var strAcad = ''
    try {
      strAcad = regkey.RegRead(
        'HKEY_CURRENT_USER\\SOFTWARE\\Autodesk\\AutoCAD\\' +
          strAcadVersion +
          '\\CurVer',
      )
    } catch (e) {
      strAcad = 'ACAD:6001-409'
    }
    if (strAcad == null || strAcad == '') {
      var objRegistry = new ActiveXObject('RegObj.Registry')
      var key = objRegistry.RegKeyFromString(
        '\\HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\AutoCAD\\' + strAcadVersion,
      )
      var subKeys = key.SubKeys
      strAcad = subKeys.Item(1).Name
    }
    var strAcadPath = ''
    try {
      strAcadPath = regkey.RegRead(
        'HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\AutoCAD\\' +
          strAcadVersion +
          '\\' +
          strAcad +
          '\\AcadLocation',
      )
    } catch (e) {
      strAcadPath = 'c:\\Program Files\\AutoCAD'
    }

    var DebugTool = config.DebugSettings
    DebugTool.Command = strAcadPath + '\\acad.exe'
    DebugTool.CommandArguments = '/ld "$(OutDir)$(ProjectName)$(TargetExt)"'

    CLTool.AdditionalUsingDirectories += strAcadPath
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function AddFilters(proj) {
  try {
    //----- Add the folders to your project
    var group = proj.Object.AddFilter('Source Files')
    group.Filter = 'cpp;c;cc;cxx;def;idl;odl'

    group = proj.Object.AddFilter('Include Files')
    group.Filter = 'h;hh;hxx'

    group = proj.Object.AddFilter('Resource Files')
    group.Filter = 'rc;ico;bmp;cur;jpg;gif'

    group = proj.Object.AddFilter('Miscellaneous Files')
    group.Filter = 'reg;rgs;mak;clw;vsdir;vsz;css;inf;vcproj;csproj'
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function PchSettings(proj) {
  try {
    //    var bDotNetModule =wizard.FindSymbol ('DOTNET_MODULE') ;
    //    if ( bDotNetModule ) {
    //- Win32
    var FileTool = GetFileTool(proj, 'Debug|Win32', 'StdAfx.cpp')
    FileTool.UsePrecompiledHeader = pchCreateUsingSpecific
    FileTool = GetFileTool(proj, 'Release|Win32', 'StdAfx.cpp')
    FileTool.UsePrecompiledHeader = pchCreateUsingSpecific
    //- x64
    var FileTool = GetFileTool(proj, 'Debug|x64', 'StdAfx.cpp')
    if (FileTool != null) FileTool.UsePrecompiledHeader = pchCreateUsingSpecific
    FileTool = GetFileTool(proj, 'Release|x64', 'StdAfx.cpp')
    if (FileTool != null) FileTool.UsePrecompiledHeader = pchCreateUsingSpecific
    //    }
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function DelFile(fso, strWizTempFile) {
  try {
    if (fso.FileExists(strWizTempFile)) {
      var tmpFile = fso.GetFile(strWizTempFile)
      tmpFile.Delete()
    }
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function CreateCustomInfFile() {
  try {
    var fso, TemplatesFolder, TemplateFiles, strTemplate
    fso = new ActiveXObject('Scripting.FileSystemObject')

    var TemporaryFolder = 2
    var tfolder = fso.GetSpecialFolder(TemporaryFolder)
    var strTempFolder = tfolder.Drive + '\\' + tfolder.Name

    var strWizTempFile = strTempFolder + '\\' + fso.GetTempName()

    var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH')
    var strInfFile = strTemplatePath + '\\Templates.inf'
    wizard.RenderTemplate(strInfFile, strWizTempFile)

    var WizTempFile = fso.GetFile(strWizTempFile)
    return WizTempFile
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function GetTargetName(strName, strProjectName) {
  try {
    //----- Set the name of the rendered file based on the template filename
    var strTarget = strName

    if (strName.substr(0, 4) == 'Root')
      strTarget = strProjectName + strName.substr(4, strName.length - 4)

    if (strName.substr(0, 3) == 'Omf')
      strTarget = strProjectName + strName.substr(3, strName.length - 3)

    return strTarget
  } catch (e) {
    throw e
  }
}

//-----------------------------------------------------------------------------
function AddFilesToCustomProj(proj, strProjectName, strProjectPath, InfFile) {
  try {
    var projItems = proj.ProjectItems

    var strTemplatePath = wizard.FindSymbol('TEMPLATES_PATH')

    var strTpl = ''
    var strName = ''

    var strTextStream = InfFile.OpenAsTextStream(1, -2)
    while (!strTextStream.AtEndOfStream) {
      strTpl = strTextStream.ReadLine()
      if (strTpl != '') {
        strName = strTpl
        var strTarget = GetTargetName(strName, strProjectName)
        var strTemplate = strTemplatePath + '\\' + strTpl
        var strFile = strProjectPath + '\\' + strTarget

        var bCopyOnly = false //----- "true" will only copy the file from strTemplate to strTarget without rendering/adding to the project
        var strExt = strName.substr(strName.lastIndexOf('.'))
        if (
          strExt == '.bmp' ||
          strExt == '.ico' ||
          strExt == '.gif' ||
          strExt == '.rtf' ||
          strExt == '.css'
        )
          bCopyOnly = true
        wizard.RenderTemplate(strTemplate, strFile, bCopyOnly)
        proj.Object.AddFile(strFile)
      }
    }
    strTextStream.Close()
  } catch (e) {
    throw e
  }
}

function RemoveFileFromProject(proj, FileName) {
  try {
    var col = proj.Object.Files
    var file = col.Item(FileName)
    if (file != null) proj.Object.RemoveFile(file)
  } catch (e) {
    throw e
  }
}

function GetFileTool(proj, Config, FileName) {
  try {
    var col = proj.Object.Files
    var file = col.Item(FileName)
    var config = file.FileConfigurations.Item(Config)
    if (config == null) return null
    var tool = config.Tool
    return tool
  } catch (e) {
    throw e
    return null
  }
}

//-----------------------------------------------------------------------------
function MakeOmfResourceProject(selProj, strProjectPath, strProjectName) {
  //- So we need a resource only DLL
  RenderAddTemplate(
    wizard,
    'OmfEnuRes.h',
    strProjectPath + '\\Enu\\Resource.h',
    false,
    false,
  )
  RenderAddTemplate(
    wizard,
    'OmfEnuRes.rc',
    strProjectPath + '\\Enu\\' + strProjectName + 'Enu.rc',
    false,
    false,
  )
  //----- Create CLSID
  var strResGUID = wizard.CreateGuid()
  var strFormattedGUID = wizard.FormatGuid(strResGUID, 0)
  wizard.AddSymbol('PROJECT_RES_GUID', strFormattedGUID)
  RenderAddTemplate(
    wizard,
    'OmfEnuRes.vcproj',
    strProjectPath + '\\Enu\\' + strProjectName + 'Enu.vcproj',
    false,
    false,
  )

  var resDllProj = selProj.DTE.Solution.AddFromFile(
    strProjectPath + '\\Enu\\' + strProjectName + 'Enu.vcproj',
    false,
  )
  var deps = selProj.DTE.Solution.SolutionBuild.BuildDependencies
  for (var i = 1; i <= deps.Count; i++) {
    var dep = deps.Item(i)
    if (dep.Project.UniqueName == selProj.UniqueName) {
      dep.AddProject(resDllProj.UniqueName)
      break
    }
  }
}
```

我是如何找到.CommandArguments 这个属性的....除了可以设置一下 vs2019 到英文版(需要单独下载语言包),

还可以通过这个[链接](<https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/kcw4dzyf(v=vs.100)?redirectedfrom=MSDN>)的介绍 vs 调试器的各种参数..

### vs2010 设置

![img](/图片/1285775-20200506204217655-450495765.png)

去掉就是 V100 平台集.

(完)
